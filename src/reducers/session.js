import {
    LOGIN_REQUEST,
    LOGIN_SUCCESS,
    LOGIN_ERROR
} from '../actions/types';

const initialState = {
    isLoading: false,
    error: null,
    isAuthenticated: false,
    name: null,
    token: null
};

function session(state = initialState, action) {
    switch (action.type) {
        case LOGIN_REQUEST:
            return {
                ...state,
                isLoading: true,
                error: null
            };
        case LOGIN_SUCCESS:
            return {
                ...state,
                isLoading: false,
                error: null,
                ...action.user
            };
        case LOGIN_ERROR:
            return {
                ...state,
                isLoading: false,
                error: action.error
            };
        default:
            return state;
    }
}

export default session;