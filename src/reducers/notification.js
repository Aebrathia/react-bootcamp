import { ADD_NOTIFICATION } from "../actions/types";

const initialState = {
    message: null,
    type: null
};

function notification(state = initialState, action) {

    switch (action.type) {
        case ADD_NOTIFICATION:
            return {
                ...action.notification
            };
        default:
            return state;
    }

}

export default notification;