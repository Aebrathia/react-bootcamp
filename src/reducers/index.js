import { combineReducers } from 'redux';

import notification from './notification';
import session from './session';

const reducers = combineReducers({
    notification,
    session
});

export default reducers;