import session from './session';
import { LOGIN_ERROR, LOGIN_REQUEST, LOGIN_SUCCESS } from "../actions/types";

describe('session reducer', () => {

    const defaultState = {
        isLoading: false,
        error: null,
        isAuthenticated: false,
        name: null,
        token: null
    };

    it('initiates empty state object on first call', () => {
        expect(session(undefined, {type: 'UNEXISTING_ACTION'})).toEqual(defaultState);
    });

    it('default case', () => {
        expect(session({
            ...defaultState,
            isLoading: true
        }, {
            type: 'UNEXISTING_ACTION'
        })).toEqual({
            ...defaultState,
            isLoading: true
        });
    });

    it('case LOGIN_REQUEST', () => {
        expect(session({
            ...defaultState,
            error: new Error('Ouch')
        }, {type: LOGIN_REQUEST})).toEqual({
            ...defaultState,
            error: null,
            isLoading: true
        });
    });

    it('case LOGIN_SUCCESS', () => {
        expect(session({
            ...defaultState,
            isLoading: true
        }, {
            type: LOGIN_SUCCESS,
            user: {
                token: 'MY_TOKEN',
                name: 'Florian'
            }
        })).toEqual({
            ...defaultState,
            isLoading: false,
            error: null,
            token: 'MY_TOKEN',
            name: 'Florian'
        });
    });

    it('case LOGIN_ERROR', () => {
        const error = new Error('Error');

        expect(session({
            ...defaultState,
            isLoading: true
        }, {
            type: LOGIN_ERROR,
            error
        })).toEqual({
            ...defaultState,
            isLoading: false,
            error
        });
    });

});