import {
    ADD_NOTIFICATION,
    LOGIN_REQUEST,
    LOGIN_SUCCESS,
    LOGIN_ERROR,
} from "./types";

export function addNotification(message, type) {
    return {
        type: ADD_NOTIFICATION,
        notification: {
            message,
            type
        }
    }
}

export function login(formData) {
    return (dispatch) => {
        dispatch({
            type: LOGIN_REQUEST
        });

        return fetch('/api/sessions', {
            method: 'POST',
            body: formData
        })
            .then(res => {
                if (res.ok) return res;
                throw new Error(res.status);
            })
            .then(res => res.json())
            .then(res => {
                dispatch({
                    type: LOGIN_SUCCESS,
                    user: res.user
                });
            })
            .catch(error => {
                dispatch({
                    type: LOGIN_ERROR,
                    error
                });
                dispatch(addNotification(error.message, 'error'));
            });
    }
}