import fetchMock from 'fetch-mock';

import {
    ADD_NOTIFICATION, LOGIN_ERROR, LOGIN_REQUEST, LOGIN_SUCCESS
} from "./types";
import { addNotification, login } from "./index";

describe('addNotification', () => {

    it('dispatches action of correct type and with notification payload', () => {
        expect(addNotification('Hello World!', 'warning')).toEqual({
           type: ADD_NOTIFICATION,
           notification: {
               message: 'Hello World!',
               type: 'warning'
           }
        });
    });

});

describe('login', () => {

    it('dispatches REQUEST action', async () => {
        fetchMock.post('/api/sessions', {
            user: {
                token: 'MY_TOKEN',
                name: 'Florian'
            }
        });
        const formDataMock = {some: 'data'};
        const dispatchMock = jest.fn();
        login(formDataMock)(dispatchMock);
        expect(dispatchMock).toHaveBeenCalledTimes(1);
        expect(dispatchMock).toHaveBeenCalledWith({
            type: LOGIN_REQUEST
        });
        await fetchMock.flush();
        expect(fetchMock.calls()[0][1].body).toBe(formDataMock);
        fetchMock.restore();
    });

    it('dispatches SUCCESS action on successful response', async () => {
        fetchMock.post('/api/sessions', {
            user: {
                token: 'MY_TOKEN',
                name: 'Florian'
            }
        });
        const dispatchMock = jest.fn();

        expect.assertions(2);

        await login({})(dispatchMock);
        expect(dispatchMock).toHaveBeenCalledTimes(2);
        expect(dispatchMock).toHaveBeenLastCalledWith({
            type: LOGIN_SUCCESS,
            user: {
                token: 'MY_TOKEN',
                name: 'Florian'
            }
        });
        fetchMock.restore();
    });

    it('dispatches ERROR action on failed response', async () => {
        const error = new Error('Network Error');
        fetchMock.post('/api/sessions', () => {throw error});
        const dispatchMock = jest.fn();

        expect.assertions(3);

        await login({})(dispatchMock);
        expect(dispatchMock).toHaveBeenCalledTimes(3);
        expect(dispatchMock).toHaveBeenCalledWith({
            type: LOGIN_ERROR,
            error
        });
        expect(dispatchMock).toHaveBeenLastCalledWith({
            type: ADD_NOTIFICATION,
            notification: {
                message: error.message,
                type: 'error'
            }
        });
        fetchMock.restore();
    });

});