import React, { Component } from 'react';
import { connect } from 'react-redux';

import LoginPassword from './LoginPassword';
import { login } from '../actions';

export class LoginForm extends Component {

    constructor(props) {
        super(props);

        this.state = {
            id: '',
            password: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    handleSubmit(event) {
        event.preventDefault();
        const formData = new FormData(event.target);
        this.props.login(formData);
    }

    render() {
        const { id, password } = this.state;

        return (
            <form onSubmit={this.handleSubmit}>
                <label htmlFor="id">Identifiant</label>
                <input type="text" name="id" value={id} onChange={this.handleChange}/><br/>
                <label htmlFor="password">Mot de passe</label>
                <LoginPassword name="password" value={password} onChange={this.handleChange}/><br/>
                <a href="/reset-password" className="LoginForm__reset-password-link">Mot de passe oublié ?</a><br/>
                <button type="submit">Connexion</button><br/>
                <a href="/register" className="LoginForm__register-link">Inscription</a>
            </form>
        )
    }

}

export default connect(null, { login })(LoginForm);