import React, { Component } from 'react';

import './LoginPage.css';
import logo from '../logo.png';
import cover from '../cover-picture.png';
import LoginForm from './LoginForm';

class LoginPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            version: null
        }
    }

    componentDidMount() {
        fetch('/api/infos')
            .then(res => res.json())
            .then(({ version }) => this.setState({ version }));
    }

    render() {
        const { version } = this.state;

        return (
            <div>
                <img src={logo} alt="Logo" className="LoginPage__logo"/>
                <img src={cover} alt="Couverture" className="LoginPage__cover" />
                <h1>Connexion</h1>
                <LoginForm/>
                <a href="/conditions-generales-d-utilisation" className="LoginPage__cgu">Conditions générales d’utilisation</a>
                <p>Quable PIM version {version}</p>
            </div>
        );
    }

}

export default LoginPage;