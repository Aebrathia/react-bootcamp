import React from 'react';
import { shallow } from 'enzyme';

import Notification from './Notification';

describe('<Notification />', () => {

    let notificationWrapper;

    beforeEach(() => {
        notificationWrapper = shallow(<Notification message="Hello World!" type="error"/>);
    });

    it('renders without crashing', () => {
        shallow(<Notification message="Hello World!" type="error" />);
    });

    it('displays a message of correct type', () => {
        expect(notificationWrapper.text()).toBe('Hello World!');
        expect(notificationWrapper.find('.Notification').hasClass('Notification_error')).toBe(true);
    });

});