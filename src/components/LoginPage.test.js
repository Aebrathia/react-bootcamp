import React from 'react';
import { shallow } from 'enzyme';
import fetchMock from 'fetch-mock';

import LoginPage from './LoginPage';
import LoginForm from './LoginForm';

fetchMock.get('/api/infos', {version: "4.28"});

describe('<LoginPage />', () => {

    let loginPageWrapper;

    beforeEach(() => {
        loginPageWrapper = shallow(<LoginPage notification={{}}/>);
    });

    it('renders without crashing', () => {
        shallow(<LoginPage notification={{}}/>);
    });

    it('displays brand logo and cover', () => {
        const logo = loginPageWrapper.find('.LoginPage__logo');
        const cover = loginPageWrapper.find('.LoginPage__cover');
        expect(logo.prop('src')).toBe('logo.png');
        expect(logo.prop('alt')).toBe('Logo');
        expect(cover.prop('src')).toBe('cover-picture.png');
        expect(cover.prop('alt')).toBe('Couverture');
    });

    it('displays correct informations', () => {
        const text = loginPageWrapper.text();
        expect(text).toMatch('Connexion');
        expect(text).toMatch('Conditions générales d’utilisation');
        expect(loginPageWrapper.find('.LoginPage__cgu').prop('href')).toBe('/conditions-generales-d-utilisation');
    });

    it('fetches and displays app version', async () => {
        await fetchMock.flush();
        loginPageWrapper.update();
        expect(loginPageWrapper.text()).toMatch('Quable PIM version 4.28');
    });

    it('contains a login form', () => {
        expect(loginPageWrapper.find(LoginForm).exists()).toBe(true);
    });

});