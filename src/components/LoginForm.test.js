import React from 'react';
import { shallow } from 'enzyme';

import { LoginForm } from './LoginForm';
import LoginPassword from './LoginPassword';

describe('<LoginForm />', () => {

    let loginFormWrapper;

    beforeEach(() => {
        loginFormWrapper = shallow(<LoginForm/>);
    });

    it('renders without crashing', () => {
        shallow(<LoginForm/>);
    });

    it('contains a form', () => {
        expect(loginFormWrapper.find('form').exists()).toBe(true);
    });

    describe('Id input', () => {

        it('is rendered with type text and has a label', () => {
            expect(loginFormWrapper.find('input[name="id"]').prop('type')).toBe('text');
            expect(loginFormWrapper.find('label[htmlFor="id"]').text()).toBe('Identifiant');
        });

        it('updates form state and its value on change', () => {
            loginFormWrapper.find('input[name="id"]').simulate('change', {target: {name: 'id', value: 'florian@quable.fr'}});
            expect(loginFormWrapper.state('id')).toBe('florian@quable.fr');
            expect(loginFormWrapper.find('input[name="id"]').prop('value')).toBe('florian@quable.fr');
        });

    });

    describe('Password input', () => {

        it('is a LoginPassword component and has a label', () => {
            expect(loginFormWrapper.find(LoginPassword).exists()).toBe(true);
            expect(loginFormWrapper.find('label[htmlFor="password"]').text()).toBe('Mot de passe');
        });

        it('updates form state and its value on change', () => {
            loginFormWrapper.find(LoginPassword).simulate('change', {target: {name: 'password', value: 'myPassword'}});
            expect(loginFormWrapper.state('password')).toBe('myPassword');
            expect(loginFormWrapper.find(LoginPassword).prop('value')).toBe('myPassword');
        });

    });

    it('displays a link to the register form', () => {
        const registerLink = loginFormWrapper.find('.LoginForm__register-link');
        expect(registerLink.prop('href')).toBe('/register');
        expect(registerLink.text()).toBe('Inscription');
    });

    it('displays a link to the reset password form', () => {
        const forgottenPasswordLink = loginFormWrapper.find('.LoginForm__reset-password-link');
        expect(forgottenPasswordLink.prop('href')).toBe('/reset-password');
        expect(forgottenPasswordLink.text()).toBe('Mot de passe oublié ?');
    });

    it('triggers login action on submit', () => {
        const loginMock = jest.fn();
        const preventDefault = jest.fn();
        loginFormWrapper = shallow(<LoginForm login={loginMock}/>);
        loginFormWrapper.find('form').simulate('submit', { preventDefault });
        expect(preventDefault).toHaveBeenCalledTimes(1);
        expect(loginMock.mock.calls[0][0]).toBeInstanceOf(FormData);
    });

});