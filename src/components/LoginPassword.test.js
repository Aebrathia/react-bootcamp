import React from 'react';
import { shallow } from 'enzyme';

import LoginPassword from './LoginPassword';

describe('<LoginPassword />', () => {

    let loginPasswordWrapper;

    beforeEach(() => {
        loginPasswordWrapper = shallow(<LoginPassword name="password"/>);
    });

    it('renders without crashing', () => {
        shallow(<LoginPassword name="password"/>);
    });

    it('has correct default state', () => {
        expect(loginPasswordWrapper.state('isVisible')).toBe(false);
    });

    it('renders a password input by default', () => {
        expect(loginPasswordWrapper.find('input').prop('type')).toBe('password');
    });

    it('renders a toggle button', () => {
        const loginPasswordToggle = loginPasswordWrapper.find('.LoginPassword__toggle');
        expect(loginPasswordToggle.exists()).toBe(true);
        expect(loginPasswordToggle.prop('type')).toBe('button');
    });

    it('passes props to input', () => {
        const onChangeMock = jest.fn();

        expect(loginPasswordWrapper.find('input').props()).toEqual({
            name: 'password',
            type: 'password'
        });
        loginPasswordWrapper.setProps({
            name: 'newName',
            onChange: onChangeMock,
            value: 'myPassword'
        });
        expect(loginPasswordWrapper.find('input').props()).toEqual({
            type: 'password',
            name: 'newName',
            onChange: onChangeMock,
            value: 'myPassword'
        });
    });

    it('toggles password visibility on click on the button', () => {
        loginPasswordWrapper.find('.LoginPassword__toggle').simulate('click');
        expect(loginPasswordWrapper.state('isVisible')).toBe(true);
        expect(loginPasswordWrapper.find('input').prop('type')).toBe('text');
        expect(loginPasswordWrapper.find('.LoginPassword__toggle').text()).toBe('Masquer le mot de passe');

        loginPasswordWrapper.find('.LoginPassword__toggle').simulate('click');
        expect(loginPasswordWrapper.state('isVisible')).toBe(false);
        expect(loginPasswordWrapper.find('input').prop('type')).toBe('password');
        expect(loginPasswordWrapper.find('.LoginPassword__toggle').text()).toBe('Afficher le mot de passe');
    });

});