import React from 'react';
import { shallow } from 'enzyme';
import { Route } from 'react-router-dom';

import { App } from './App';
import Notification from './Notification';
import LoginPage from "./LoginPage";
import PlanningPage from "./PlanningPage";

describe('<App />', () => {

    let appWrapper;

    beforeEach(() => {
        appWrapper = shallow(<App notification={{}} />);
    });

    it('renders without crashing', () => {
        shallow(<App notification={{}} />);
    });

    it('displays Notification when notification prop is passed', () => {
        expect(appWrapper.find(Notification).exists()).toBe(false);
        appWrapper.setProps({
            notification: {
                message: "Hello World!",
                type: "success"
            }
        });
        expect(appWrapper.find(Notification).props()).toEqual({
            message: "Hello World!",
            type: "success"
        });
    });

    it('displays the PlanningPage component on correct route', () => {
        const route = appWrapper.find(Route).at(0);
        expect(route.prop('path')).toBe('/planning');
        expect(route.prop('component')).toBe(PlanningPage);
    });

    it('displays the LoginPage component on correct route', () => {
        const route = appWrapper.find(Route).at(1);
        expect(route.prop('path')).toBe('/login');
        expect(route.prop('component')).toBe(LoginPage);
    });

});