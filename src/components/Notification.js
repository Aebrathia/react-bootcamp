import React from 'react';
import PropTypes from 'prop-types';

const Notification = ({ message, type }) => (
    <div className={`Notification Notification_${type}`}>
        {message}
    </div>
);

Notification.propTypes = {
    message: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
};

export default Notification;