import React, { Component } from 'react';
import PropTypes from 'prop-types';

class LoginPassword extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isVisible: false
        };

        this.toggleVisibility = this.toggleVisibility.bind(this);
    }

    toggleVisibility() {
        this.setState({ isVisible: !this.state.isVisible });
    }

    render() {
        const { isVisible } = this.state;

        return (
            <span>
                <input type={isVisible ? 'text' : 'password'} {...this.props}/>
                <button type="button" className="LoginPassword__toggle" onClick={this.toggleVisibility}>
                    {isVisible
                        ? 'Masquer '
                        : 'Afficher '
                    }
                    le mot de passe
                </button>
            </span>
        );
    }

}

LoginPassword.propTypes = {
    name: PropTypes.string.isRequired
};

export default LoginPassword;