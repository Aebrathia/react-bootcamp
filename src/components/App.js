import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Switch, Route } from 'react-router-dom';

import LoginPage from './LoginPage';
import PlanningPage from './PlanningPage';
import Notification from './Notification';

export const App = ({ notification }) => (
    <div>
        {notification.message &&
            <Notification message={notification.message} type={notification.type} />
        }
        <Switch>
            <Route path="/planning" component={PlanningPage}/>
            <Route path="/login" component={LoginPage}/>
        </Switch>
    </div>
);

App.propTypes = {
    notification: PropTypes.shape({
        message: PropTypes.string,
        type: PropTypes.string
    }).isRequired
};

const mapStateToProps = (state) => {
    return {
        notification: state.notification
    }
};

export default connect(mapStateToProps)(App);
