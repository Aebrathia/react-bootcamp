#!/usr/bin/env bash
docker run -u node \
-v $(pwd):/home/node/app \
-w /home/node/app \
-p 3000:3000 \
-p 3001:3001 \
-it node:8.11.2 bash