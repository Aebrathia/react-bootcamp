const express = require('express');
const app = express();
const api = express.Router();

api.get('/infos', (req, res) => res.send({
    version: "4.28"
}));

api.post('/sessions', (req, res) => {
    res.send({
        user: {
            token: 'MY_TOKEN',
            name: 'Florian'
        }
    });
});

app.use('/api/', api);

app.listen(3001, () => console.log('Listening on port 3001'));